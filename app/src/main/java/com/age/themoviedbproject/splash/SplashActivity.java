package com.age.themoviedbproject.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.afollestad.materialdialogs.MaterialDialog;
import com.age.themoviedbproject.movie.MovieActivity;

import androidx.appcompat.app.AppCompatActivity;



public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private MaterialDialog materialDialog;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // UI in styles.xml


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MovieActivity.class));
                finish();
            }
        }, 1000);

    }

}


package com.age.themoviedbproject.root;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by adri on 23/11/2018.
 */
public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }



    private void initialize() {
        Fresco.initialize(this);
    }
}

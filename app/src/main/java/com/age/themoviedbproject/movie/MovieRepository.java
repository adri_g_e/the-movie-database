package com.age.themoviedbproject.movie;

import com.age.themoviedbproject.http.apis.TMDBApi;
import com.age.themoviedbproject.http.do_themoviedb.PopularMovies;

import io.reactivex.Observable;

/**
 * Created by adri on 23/11/2018.
 */
public class MovieRepository implements Repository {

    private TMDBApi tmdbApi;


    public MovieRepository(TMDBApi tmdbApi) {
        this.tmdbApi = tmdbApi;
    }

    @Override
    public Observable<PopularMovies> getPopularMovies(int page) {
        Observable<PopularMovies> popularMoviesObservable = tmdbApi.getPopularMovies(page);
        return popularMoviesObservable;
    }

    @Override
    public Observable<PopularMovies> getSearchedMovies(int page, String query) {
        Observable<PopularMovies> popularMoviesObservable = tmdbApi.searchMovies(page, query);
        return popularMoviesObservable;
    }
}

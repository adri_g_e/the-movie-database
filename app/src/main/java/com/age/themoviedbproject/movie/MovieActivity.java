package com.age.themoviedbproject.movie;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.age.themoviedbproject.R;
import com.age.themoviedbproject.http.do_themoviedb.Movie;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;



public class MovieActivity extends AppCompatActivity implements MovieMVP.View,
        MaterialSearchView.OnQueryTextListener,
        MaterialSearchView.SearchViewListener,
        SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener {



    private static final String TAG = MovieActivity.class.getSimpleName();

    private LinearLayoutManager layoutManager;
    private MovieAdapter movieAdapter;
    private List<Movie> movieList = new ArrayList<>();

    private boolean isLoading = false;
    private boolean isLastPage = false;

    private final int PAGINATION_MARGIN_ITEMS = 2; // El margen antes de realizar una nueva llamada para obtener más items. Si tengo 10 items y cuando se muestre el 8º quiero obtener más, debería valer 2.



    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rootLinearLayout)
    LinearLayout rootLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;
    @BindView(R.id.btnRefresh)
    MaterialButton btnRefresh;

    @Inject
    MovieMVP.Presenter presenter;

    private String query; // Query para búsqueda, si es null o vacío devolverá las más populares.
    private int scrollItemPosition; // Posición de scroll en la que estamos. Para poder recuperar la situación del usuario cuando recuperamos la lista desde caché.


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        ButterKnife.bind(this);

        DaggerMovieComponent.builder().build().inject(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Define layout para RecyclerView.
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);

        movieAdapter = new MovieAdapter(movieList, this);
        recyclerView.setAdapter(movieAdapter);


        // Para habilitar la paginación
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        swipeRefresh.setOnRefreshListener(this);
        btnRefresh.setOnClickListener(this);

        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        recyclerView.setVisibility(View.GONE); // Lo ocultamos para que no muestre el footer la primera vez que carga, ya que aparece el swipe.

        searchView.setOnQueryTextListener(this);
        searchView.setOnSearchViewListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.loadMovies(query);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.setView(null);
        presenter.unSubscribe();
        movieList.clear();
        movieAdapter.notifyDataSetChanged();
    }











    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            if (!searchView.isSearchOpen())
                scrollItemPosition = firstVisibleItemPosition; // Guardamos la posición en la que está el usuario si la searchView está cerrada.

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount - PAGINATION_MARGIN_ITEMS
                        && firstVisibleItemPosition != RecyclerView.NO_POSITION && movieList.size() > 0) {
                        presenter.loadMovies(query);
                }
            }
        }
    };












    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        Log.d(TAG, "onQueryTextChange: filtrando por '" + query + "'.");
        this.query = query;
        clearList();
        if (!query.isEmpty())
            presenter.loadMovies(this.query);
        return false;
    }



    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }



    @Override
    public void onSearchViewShown() {
        movieList.clear();
        movieAdapter.notifyDataSetChanged();
    }


    @Override
    public void onSearchViewClosed() {
        query = null;
        presenter.loadFromCache();
        if (this.scrollItemPosition <= this.movieList.size() && this.scrollItemPosition != RecyclerView.NO_POSITION)
            recyclerView.scrollToPosition(this.scrollItemPosition); // Hacemos scroll hasta la posición en la que estaba el usuuario antes de abrir la searchView.
    }











    @Override
    public void updateData(List<Movie> movies) {
        movieList.addAll(movies);
        movieAdapter.notifyDataSetChanged();
        showList();
    }


    @Override
    public void updateSearchedData(List<Movie> movies) {
        clearList();
        movieList.addAll(movies);
        movieAdapter.notifyDataSetChanged();
        showList();
    }


    @Override
    public void showMessage(String message) {
        if (movieAdapter.getItemCount() == 1) { // Si solo tiene el footer.
            swipeRefresh.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        Snackbar.make(rootLinearLayout, message, Snackbar.LENGTH_SHORT).show();
    }













    @Override
    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;

        if (movieAdapter.getItemCount() == 0)
            this.swipeRefresh.setRefreshing(isLoading);
        else
            this.swipeRefresh.setRefreshing(false);

        if (isLoading)
            movieAdapter.showProgress();
        else
            movieAdapter.hideProgress();
    }



    @Override
    public void setIsLastPage(boolean isLastPage) {
        this.isLastPage = isLastPage;
        movieAdapter.hideProgress();
    }











    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            moveTaskToBack(true);
        }
    }



    @Override
    public void onRefresh() {
        movieList.clear();
        if (searchView.isSearchOpen())
            searchView.closeSearch();
        else
            presenter.refreshMovies();
    }



    @Override
    public void onClick(View v) {
        emptyView.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);
        swipeRefresh.setRefreshing(true);
        onRefresh();
    }









    /**
     * Limpia la lista y refresca el adaptador.
     */
    private void clearList() {
        movieList.clear();
        movieAdapter.notifyDataSetChanged();
    }

    /**
     * Muestra la lista y el swipe refresh layout, además esconde la 'empty view'.
     */
    private void showList() {
        if (swipeRefresh.getVisibility() != View.VISIBLE || recyclerView.getVisibility() != View.VISIBLE) {
            emptyView.setVisibility(View.GONE);
            swipeRefresh.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}



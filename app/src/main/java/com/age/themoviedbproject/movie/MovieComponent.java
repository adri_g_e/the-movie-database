package com.age.themoviedbproject.movie;


import com.age.themoviedbproject.http.apimodules.TMDBModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by adri on 22/11/2018.
 */
@Singleton
@Component(modules = {MovieModule.class, TMDBModule.class})
public interface MovieComponent {

    void inject(MovieActivity activity);

    void inject(MovieRepository movieRepository);

}

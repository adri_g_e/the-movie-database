package com.age.themoviedbproject.movie;


import android.util.Log;

import com.age.themoviedbproject.http.do_themoviedb.Movie;
import com.age.themoviedbproject.http.do_themoviedb.PopularMovies;

import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MoviePresenter implements MovieMVP.Presenter {



    private final String TAG = MoviePresenter.class.getSimpleName();

    @Nullable // Indica que estos valores pueden ser null.
    private MovieMVP.View view;
    private MovieMVP.Model model;

    private Disposable disposable = null; // Objeto subscription

    private int currentPage = 0; // Variable para control de paginación, página actual.
    private int totalPages = -1; // Variable para control de paginación, páginas máximas permitidas.

    private int currentFilterPage = 0;
    private int totalFilterPages = -1;

    private boolean errorShowed = false;

    private String query = "";





    public MoviePresenter(MovieMVP.Model model) {
        this.model = model;
    }


    @Override
    public void setView(MovieMVP.View view) {
        this.view = view;
    }




    /**
     * Descarga de películas.
     * @param query en caso de búsqueda.
     */
    @Override
    public void loadMovies(@Nullable  String query) {
        if (query != null)
            searchMovies(query);
        else
            loadPopularMovies();
    }






    public void loadPopularMovies() {
        currentPage++;


        if (!existMorePages()) {
            currentPage--;
            return;
        }


        if (view != null)
            view.setIsLoading(true);


        disposable = model.getDataMovies(currentPage)
                .concatMap(new Function<PopularMovies, Observable<List<Movie>>>() {
                    @Override
                    public Observable<List<Movie>> apply(PopularMovies popularMovies) {
                        totalPages = popularMovies.getTotalPages();
                        return Observable.just(popularMovies.getMovies());
                    }
                })
                .subscribeOn(Schedulers.io()) // Se realiza en hilo segundo plano.
                .observeOn(AndroidSchedulers.mainThread()) // Notifica el resultado en hilo principal, los callbacks de la subscripción son en mainthread y podemos acceder a la vista directamente.
                .subscribeWith(new DisposableObserver<List<Movie>>() {
                    @Override
                    public void onNext(List<Movie> movies) {
                        if (view != null) {
                            view.updateData(movies);
                            errorShowed = false;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            if (!errorShowed) {
                                view.showMessage("Se ha producido un error al intentar obtener la lista de películas.");
                            }
                            view.setIsLoading(false);
                            errorShowed = true;
                        }

                        if (currentPage > 0) // Si ha fallado se resta una página ya que suma 1 al inicio del método.
                            currentPage--;

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete:  página " +currentPage + " de " + totalPages + " descargada.");
                        view.setIsLoading(false);
                        if (currentPage == totalPages)
                            view.setIsLastPage(true);
                    }
                });
    }







    public void searchMovies(String query) {
        if (!this.query.equalsIgnoreCase(query)) {
            currentFilterPage = 0;
            totalFilterPages = -1;
            if (view != null)
                view.setIsLastPage(false);
        }

        this.query = query;

        currentFilterPage++;

        if (!existMoreSearchPages()) {
            currentFilterPage--;
            return;
        }


        if (view != null)
            view.setIsLoading(true);

        disposable = model.getSearchDataMovies(currentFilterPage, query)
                .debounce(300, TimeUnit.MILLISECONDS) // Espera el tiempo asignado antes de lanzar la consulta. Si antes de empezar recibe otra consula, ignora la anterior y espera para la nueva consulta. Esto nos asegura que si el usuario escribe muy rápido no se lanze la consulta hasta que acabe.
                .distinctUntilChanged() // Para evitar las llamadas de red duplicadas
                .concatMap(new Function<PopularMovies, Observable<List<Movie>>>() {
                    @Override
                    public Observable<List<Movie>> apply(PopularMovies popularMovies)  {
                        totalFilterPages = popularMovies.getTotalPages();
                        return Observable.just(popularMovies.getMovies());
                    }
                })
                .subscribeOn(Schedulers.io()) // Se realiza en hilo segundo plano.
                .observeOn(AndroidSchedulers.mainThread()) // Notifica el resultado en hilo principal, los callbacks de la subscripción son en mainthread y podemos acceder a la vista directamente.
                .subscribeWith(new DisposableObserver<List<Movie>>() {
                    @Override
                    public void onNext(List<Movie> movies) {
                        if (view != null) {
                            if (currentFilterPage == 1)
                                view.updateSearchedData(movies);
                            else
                                view.updateData(movies);

                            errorShowed = false;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            if (!errorShowed) {
                                view.showMessage("Se ha producido un error al intentar obtener la lista de películas con '" + query + "'.");
                            }
                            view.setIsLoading(false);
                            errorShowed = true;
                        }

                        if (currentFilterPage > 0) // Si ha fallado se resta una página ya que suma 1 al inicio del método.
                            currentFilterPage--;

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete:  página " + currentFilterPage + " de " + totalFilterPages + " descargada.");
                        view.setIsLoading(false);
                        if (currentFilterPage == totalFilterPages)
                            view.setIsLastPage(true);
                    }
                });

    }







    @Override
    public void loadFromCache() {
        // Si no es la última página informa a la vista ya que en la búsqueda de una película se ha podido cambiar el valor.
        if (currentPage < totalPages)
            view.setIsLastPage(false);

        List<Movie> cacheMovieList = model.getMoviesFromCache();
        view.updateData(cacheMovieList);
    }







    @Override
    public void refreshMovies() {
        currentPage = 0;
        totalPages = -1;
        currentFilterPage = 0;
        totalFilterPages = -1;
        errorShowed = false;
        if (view != null)
            view.setIsLastPage(false);
        loadMovies(null);
    }



    private boolean existMorePages() {
        return currentPage <= totalPages || totalPages == -1;
    }



    private boolean existMoreSearchPages() {
        return currentFilterPage <= totalFilterPages || totalFilterPages == -1;
    }


    @Override
    public void unSubscribe() {
        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();
    }




}

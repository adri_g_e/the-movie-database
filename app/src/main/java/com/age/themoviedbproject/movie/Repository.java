package com.age.themoviedbproject.movie;


import com.age.themoviedbproject.http.do_themoviedb.PopularMovies;

import io.reactivex.Observable;

/**
 * Created by adri on 23/11/2018.
 */
public interface Repository {

    Observable<PopularMovies> getPopularMovies(int page);



    Observable<PopularMovies> getSearchedMovies(int page, String query);
}

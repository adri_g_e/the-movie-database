package com.age.themoviedbproject.movie;


import com.age.themoviedbproject.http.do_themoviedb.Movie;
import com.age.themoviedbproject.http.do_themoviedb.PopularMovies;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class MovieModel implements MovieMVP.Model {

    private Repository movieRepository;

    private List<Movie> movieList = new ArrayList<>();


    public MovieModel(Repository movieRepository) {
        this.movieRepository = movieRepository;
    }



    @Override
    public Observable<PopularMovies> getDataMovies(int page) {
        return movieRepository.getPopularMovies(page).doOnNext(popularMovies -> {
            movieList.addAll(popularMovies.getMovies());
        });
    }

    @Override
    public Observable<PopularMovies> getSearchDataMovies(int page, String query) {
        return movieRepository.getSearchedMovies(page, query);
    }



    @Override
    public List<Movie> getMoviesFromCache() {
        return movieList;
    }


}
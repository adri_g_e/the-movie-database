package com.age.themoviedbproject.movie;


import com.age.themoviedbproject.http.apis.TMDBApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MovieModule {

    @Provides
    public MovieMVP.Presenter provideMoviePresenter(MovieMVP.Model model) {
        return new MoviePresenter(model);
    }


    @Provides
    public MovieMVP.Model provideMovieModel(Repository movieRepository) {
        return new MovieModel(movieRepository);
    }


    @Singleton
    @Provides
    public Repository provideMovieRepository(TMDBApi tmdbApi) {
        return new MovieRepository(tmdbApi);
    }
}
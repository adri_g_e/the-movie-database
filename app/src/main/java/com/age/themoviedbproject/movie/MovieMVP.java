package com.age.themoviedbproject.movie;


import com.age.themoviedbproject.http.do_themoviedb.Movie;
import com.age.themoviedbproject.http.do_themoviedb.PopularMovies;

import java.util.List;

import io.reactivex.Observable;

public interface MovieMVP {


    // // Métodos que implementa la UI (Activity).
    interface View {

        /**
         * Añade la lista de películas al adaptador y lo refresca para que se visualicen.
         * @param movies Lista del DO Movie.
         */
        void updateData(List<Movie> movies);

        /**
         * Limpia la lista y añade la nueva lista de películas al adaptador y lo refresca para que se visualicen.
         * @param movies Lista del DO Movie.
         */
        void updateSearchedData(List<Movie> movies);

        /**
         * Muestra mensaje en pantalla.
         * @param message
         */
        void showMessage(String message);

        /**
         * Le indica a la vista que el presentador está obteniendo nuevos datos del modelo.
         * @param isLoading Boleano que indica si se están cargando más resultados.
         */
        void setIsLoading(boolean isLoading);

        /**
         * Informa a la vista de que ya no hay más páginas para poder realizar consultas.
         * @param isLastPage
         */
        void setIsLastPage(boolean isLastPage);

    }


    // Todos los métodos necesarios para interactuar con la vista y el modelo.
    interface Presenter {

        /**
         * Asignación de la vista con la que trabaja este presentador.
         */
        void setView(MovieMVP.View view);

        /**
         * LLama a la descarga de películas o el filtraje según el valor de la variable "isFiltering".
         * @param query en caso de búsqueda.
         */
        void loadMovies(String query);


        /**
         * Reinicia las variables de control para que cargue de nuevo la página 1.
         */
        void refreshMovies();


        /**
         * Obtiene las películas previamente descargadas y alamacenadas en la cache.
         */
        void loadFromCache();

        /**
         * Unsubscribe de RxJava.
         */
        void unSubscribe();

    }


    // Incluye toda la lógica de negocio (business).
    interface Model {

        /**
         * Devuelve un observable tras una llamada a la API para obtener la página de películas solicitada.
         * @param page Número de página solicitada.
         * @return Objeto observable RXJava2 del DO PopularMovies.
         */
        Observable<PopularMovies> getDataMovies(int page);



        /**
         * Devuelve un observable tras una llamada a la API para obtener las películas que contengan la query recibida.
         * @param page Número de página solicitada.
         * @param query Cadena para la búsqueda de películas.
         * @return Objeto observable RXJava2 del DO PopularMovies.
         */
        Observable<PopularMovies> getSearchDataMovies(int page, String query);


        /**
         * Obtiene la lista de películas de la caché.
         * @return Lisa del DO Movie.
         */
        List<Movie> getMoviesFromCache();

    }
}

package com.age.themoviedbproject.movie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.age.themoviedbproject.R;
import com.age.themoviedbproject.http.do_themoviedb.Movie;
import com.facebook.drawee.view.SimpleDraweeView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;



/**
 * Created by adri on 22/11/2018
 */


public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private List<Movie> items;

    private final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500/";
    private SimpleDateFormat format;

    private static final int VIEWTYPE_MOVIE = 1;
    private static final int VIEWTYPE_LOADER = 2;


    private LoaderViewHolder footerHolder;


    public MovieAdapter(List<Movie> items, Context context) {
        this.items = items;
        this.context = context;
        format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    }



    @Override
    public int getItemViewType(int position) {
        if (position == items.size()) {
            return VIEWTYPE_LOADER;
        } else  {
            return VIEWTYPE_MOVIE;
        }
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEWTYPE_MOVIE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
            return new MovieViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_footer, parent, false);
            return new LoaderViewHolder(v);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MovieViewHolder) {

            Movie item = items.get(position);
            ((MovieViewHolder) holder).set(item);

        } else if (holder instanceof LoaderViewHolder) {
            footerHolder = ((LoaderViewHolder) holder);
        }
    }





    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }

        if (items.size() == 0) {
            return 0;
        }

        // Add extra view to show the footer view
        return items.size() + 1;
    }






    public void hideProgress() {
        if (footerHolder != null) {
            footerHolder.progressBar.setVisibility(View.GONE);
            footerHolder.tvNoResults.setVisibility(View.VISIBLE);
        }

    }

    public void showProgress() {
        if (footerHolder != null) {
            footerHolder.tvNoResults.setVisibility(View.GONE);
            footerHolder.progressBar.setVisibility(View.VISIBLE);
        }
    }





    public class MovieViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img)
        SimpleDraweeView img;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvYear)
        TextView tvYear;
        @BindView(R.id.tvOverview)
        TextView tvOverview;


        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void set(Movie movie) {
            //UI setting code
            img.setImageURI(BASE_URL_IMAGE + movie.getPosterPath());
            tvTitle.setText(movie.getTitle());

            int year = getYear(movie.getReleaseDate());
            if (year != -1) {
                tvYear.setText(String.valueOf(year));
                tvYear.setVisibility(View.VISIBLE);
            } else
                tvYear.setVisibility(View.GONE);

            if (movie.getOverview().isEmpty())
                tvOverview.setText(context.getString(R.string.without_description));
            else
                tvOverview.setText(movie.getOverview());
        }


        /**
         * Obtiene el año a partir de una fecha String en formato americano.
         * @param strDate String date with US format (2012-05-20).
         * @return (int) Devuelve el año si la fecha es válida, en caso contrario devuelve -1.
         */
        private int getYear(String strDate) {
            try {
                Date date = format.parse(strDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                return calendar.get(Calendar.YEAR);
            } catch (ParseException e) {
                e.printStackTrace();
                return -1;
            }
        }
    }






    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.loader)
        ProgressBar progressBar;
        @BindView(R.id.tvNoResults)
        TextView tvNoResults;

        public LoaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }




}
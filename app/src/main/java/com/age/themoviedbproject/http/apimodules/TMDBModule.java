package com.age.themoviedbproject.http.apimodules;

import com.age.themoviedbproject.http.apis.TMDBApi;

import java.io.IOException;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by adri on 22/11/2018.
 */

@Module
public class TMDBModule {


    private final String BASE_URL_THE_MOVIE_DB= "https://api.themoviedb.org/3/";
    private final String API_KEY_THE_MOVIE_DB = "93aea0c77bc168d8bbce3918cefefa45";


    // Configuramos un interceptor personalizado (aunque no es necesario...pero es interesante).
    @Provides
    public OkHttpClient provideHttpClientMovieDB() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        // Mediante un interceptor añadimos siempre la key cómo parámetro en la query antes de realizar la petición. También añadimos el parámetro "language" para que devuelva los resultados en español.
                        Request request = chain.request();
                        HttpUrl httpUrl = request.url();

                        HttpUrl finalHttpUrl = httpUrl.newBuilder()
                                .addQueryParameter("api_key", API_KEY_THE_MOVIE_DB)
                                .addQueryParameter("language", "es")
                                .build();
                        Request.Builder requestBuilder = request.newBuilder();

                        Request finalRequest = requestBuilder.url(finalHttpUrl).build();

                        return chain.proceed(finalRequest);
                    }
                }).build();
        return okHttpClient;
    }


    @Provides
    public Retrofit provideRetrofit(String baseURL, OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client) // Le pasamos el interceptor personalizado.
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit;
    }


    @Provides
    public TMDBApi provideTMDBService() {
        return provideRetrofit(BASE_URL_THE_MOVIE_DB, provideHttpClientMovieDB()).create(TMDBApi.class);
    }

}

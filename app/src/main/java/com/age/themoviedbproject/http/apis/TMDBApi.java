package com.age.themoviedbproject.http.apis;

import com.age.themoviedbproject.http.do_themoviedb.PopularMovies;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by adri on 22/11/2018.
 */
public interface TMDBApi {

    @GET("movie/popular")
    Observable<PopularMovies> getPopularMovies(
            @Query("page") int page);



    @GET("search/movie")
    Observable<PopularMovies> searchMovies(
            @Query("page") int page,
            @Query("query") String query);

}

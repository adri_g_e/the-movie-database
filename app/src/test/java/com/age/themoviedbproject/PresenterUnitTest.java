package com.age.themoviedbproject;

import com.age.themoviedbproject.movie.MovieMVP;
import com.age.themoviedbproject.movie.MoviePresenter;

import org.junit.Before;

import static org.mockito.Mockito.mock;

/**
 * Created by adri on 26/11/2018.
 */
public class PresenterUnitTest {

    MoviePresenter moviePresenter;


    // Objetos simulados por Mockito.
    MovieMVP.Model mockedModel;
    MovieMVP.View mockedView;



    // Se ejecuta al arrnancar, inicia objetos, asigna estados, etc... Antes de ejecutar los test.
    @Before
    public void initialization() {
        mockedModel = mock(MovieMVP.Model.class);
        mockedView = mock(MovieMVP.View.class);

        moviePresenter = new MoviePresenter(mockedModel);
        moviePresenter.setView(mockedView);
    }



    /*
    @Test
    ...
    */

}
